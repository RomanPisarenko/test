package org.example;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis());

        List<Golfer> golfers = Arrays.asList(
                new Golfer("Джек", "Никлаус", 68),
                new Golfer("Тайгер", "Вудс", 70),
                new Golfer("Том", "Уотсон", 70),
                new Golfer("Тай", "Уэбб", 68),
                new Golfer("Бубба", "Уотсон", 70)
        );

        golfers.sort(null);

        System.out.println(golfers);

    }
}

